package ru.pavel.themoviedb.pojo.DetailsAboutTV.Video;

/**
 * Created by Pavel on 27.04.2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoTV {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("results")
    @Expose
    private List<ResultVideoTV> results = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ResultVideoTV> getResults() {
        return results;
    }

    public void setResults(List<ResultVideoTV> results) {
        this.results = results;
    }
}
