package ru.pavel.themoviedb.pojo.Films;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TrilerFilm {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("results")
    @Expose
    private List<ResultTtiler> resultTtiler = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ResultTtiler> getResultTtiler() {
        return resultTtiler;
    }

    public void setResultTtiler(List<ResultTtiler> resultTtiler) {
        this.resultTtiler = resultTtiler;
    }

}
