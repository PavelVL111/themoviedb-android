package ru.pavel.themoviedb.pojo.Films;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pavel on 30.03.2017.
 */

public class ResultParcelable implements Parcelable {
    Result result;

    protected ResultParcelable(Parcel in) {
    }

    protected ResultParcelable(Result res) {
        this.result = res;
    }

    public Result getResult () {
        return result;
    }

    public static final Creator<ResultParcelable> CREATOR = new Creator<ResultParcelable>() {
        @Override
        public ResultParcelable createFromParcel(Parcel in) {
            return new ResultParcelable(in);
        }

        @Override
        public ResultParcelable[] newArray(int size) {
            return new ResultParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
