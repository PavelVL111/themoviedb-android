package ru.pavel.themoviedb.Films;
//TabPopularFilms

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.pavel.themoviedb.ApiInterface;
import ru.pavel.themoviedb.R;
import ru.pavel.themoviedb.pojo.Films.Result;
import ru.pavel.themoviedb.pojo.Films.PopularFilms;


public class TabPopularFilms extends Fragment {

    private Context context;
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerViewNew;
    private LinearLayoutManager mLayoutManager;
    private LinearLayoutManager mLayoutManagerNew;
    //private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerAdapter mAdapter;
    private RecyclerAdapter mAdapterNew;
    private ArrayList<String> myDataset;
    private ApiInterface service;
    private String BASE_URL = "https://api.themoviedb.org";
    private Uri uri = Uri.parse("https://image.tmdb.org/t/p/w500/");
    private String page;
    private TextView textView;
    private Result result1;
    private List<Result> listResult;
    private PopularFilms result;
    private ProgressBar progressBar;
    private Toolbar toolbartab;
    private int totalItem;
    private int lastVisibleItem = 0;
    private int pg = 1;
    private int lastValue = 0;
    private View view;

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            totalItem = mLayoutManager.getItemCount();
            lastVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
            if (lastValue != lastVisibleItem){
                if ((totalItem - 3) == lastVisibleItem){
                    pg++;
                    setPageCall(pg + "");
                }
                lastValue = lastVisibleItem;
            }

        }
    };


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_tab_popular_films, container, false);

        runClient("1");
        return view;
    }

    private void runClient(String page) {
        Retrofit client = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = client.create(ApiInterface.class);
        setFirstPageCall(page);
    }

    private void setFirstPageCall(String page) {
        Call<PopularFilms> call = service.getFilm(page);
        startCall(call);
    }

    private void startCall(Call<PopularFilms> call) {
        call.enqueue(new Callback<PopularFilms>() {
            @Override
            public void onResponse(Call<PopularFilms> call, Response<PopularFilms> response) {
                if (response.isSuccessful()) {
                    result = response.body();
                    listResult = result.getResults();

                    mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
                    mRecyclerView.setHasFixedSize(true);

                    mLayoutManager = new LinearLayoutManager(view.getContext());
                    mRecyclerView.setLayoutManager(mLayoutManager);

                    mAdapter = new RecyclerAdapter(listResult, view.getContext(), service);
                    mRecyclerView.setAdapter(mAdapter);
                    mRecyclerView.addOnScrollListener(onScrollListener);
                    progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
                    progressBar.setVisibility(ProgressBar.INVISIBLE);

                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }


            @Override
            public void onFailure(Call<PopularFilms> call, Throwable t) {

            }

        });
    }

    private void setPageCall(String page) {
        Call<PopularFilms> call = service.getFilm(page);
        addCall(call);
    }

    private void addCall(Call<PopularFilms> call) {
        call.enqueue(new Callback<PopularFilms>() {
            @Override
            public void onResponse(Call<PopularFilms> call, Response<PopularFilms> response) {
                if (response.isSuccessful()) {
                    result = response.body();
                    mAdapter.add(result);
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<PopularFilms> call, Throwable t) {

            }

        });
    }


}