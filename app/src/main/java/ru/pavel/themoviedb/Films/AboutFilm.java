package ru.pavel.themoviedb.Films;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.pavel.themoviedb.ApiInterface;
import ru.pavel.themoviedb.Credits.RecyclerAdapterCredits;
import ru.pavel.themoviedb.R;
import ru.pavel.themoviedb.pojo.DetailsAboutFilms.DetailsFilms;
import ru.pavel.themoviedb.pojo.DetailsAboutFilms.Genre;
import ru.pavel.themoviedb.pojo.DetailsAboutFilms.ProductionCompany;
import ru.pavel.themoviedb.pojo.Films.Result;
import ru.pavel.themoviedb.pojo.Films.ResultTtiler;
import ru.pavel.themoviedb.pojo.Films.TrilerFilm;
import ru.pavel.themoviedb.pojo.Team.Credits;

public class AboutFilm extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{
    private int position;
    private List<Result> listResult;
    private String BASE_URL = "https://api.themoviedb.org";
    private TextView textView;
    private TrilerFilm trilerFilm;
    private YouTubePlayer.OnInitializedListener oil;
    private String idTriler;
    private String key;
    private Intent intent;
    private ResultTtiler resultTtiler;
    private String id;
    private DetailsFilms detailsFilms ;
    private Context context = this;
    private Uri uri = Uri.parse("https://image.tmdb.org/t/p/w500");
    private ImageView imageView;
    private ImageView imagePlay;
    private TextView overview;
    private TextView orignalTitle;
    private TextView releaseDate;
    private TextView voteAverage;
    private TextView geners;
    private TextView tagline;
    private TextView budget;
    private TextView productionCompanies;
    private YouTubePlayerView youTubePlayerView;
    private LinearLayout linearLayoutDark;
    private LinearLayout linearLayoutWhite;
    private RecyclerAdapterCredits RAdapterCredits;
    private Credits credits;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_about_film);

        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youTubePlayerView);
        textView = (TextView) findViewById(R.id.title);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        intent = getIntent();
        Result result = intent.getParcelableExtra("parcelable");
        //id = intent.getStringExtra("id");
        id = result.getId().toString();
        textView.setText(result.getTitle());
        oil = this;
        Retrofit client = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final ApiInterface service = client.create(ApiInterface.class);
        context = this;
        imageView = (ImageView) findViewById(R.id.backdrop_path);
        imagePlay = (ImageView) findViewById(R.id.play);
        overview = (TextView) findViewById(R.id.overview);
        voteAverage = (TextView) findViewById(R.id.vote_average);
        linearLayoutDark = (LinearLayout) findViewById(R.id.dark);
        orignalTitle = (TextView) findViewById(R.id.original_title_new);
        releaseDate = (TextView) findViewById(R.id.release_date);
        linearLayoutWhite = (LinearLayout) findViewById(R.id.white);
        geners = (TextView) findViewById(R.id.genres);
        tagline = (TextView) findViewById(R.id.tagline);
        budget = (TextView) findViewById(R.id.budget);
        productionCompanies = (TextView) findViewById(R.id.production_companies);
        imagePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setVisibility(View.INVISIBLE);
                imagePlay.setVisibility(View.INVISIBLE);
                linearLayoutDark.setVisibility(View.INVISIBLE);
                youTubePlayerView.setVisibility(View.VISIBLE);
                loadTrailer(service);
            }
        });
        getDetilsFilm(service);
        getCreditsFilm(service);
        youTubePlayerView.setVisibility(View.INVISIBLE);
       // loadTrailer(service);

    }

    private void getDetilsFilm(ApiInterface service) {
        Call<DetailsFilms> call = service.getDetilsFilm(id);
        call.enqueue(new Callback<DetailsFilms>() {
            @Override
            public void onResponse(Call<DetailsFilms> call, Response<DetailsFilms> response) {
                if (response.isSuccessful()) {
                    detailsFilms = response.body();
                    overview.setText(detailsFilms.getOverview());
                    voteAverage.setText(detailsFilms.getVoteAverage().toString());
                    orignalTitle.setText(detailsFilms.getOriginalTitle());
                    releaseDate.setText(detailsFilms.getReleaseDate());
                    geners.setText("Жанр: " + getGenrsInString(detailsFilms.getGenres()));
                    tagline.setText("Слоган: " + detailsFilms.getTagline());
                    budget.setText("Бюджет: " + detailsFilms.getBudget().toString() + "$");
                    productionCompanies.setText("Студия: " + getProductionCompaniesInString(detailsFilms.getProductionCompanies()));
                    linearLayoutWhite.setVisibility(View.INVISIBLE);

                    Picasso.with(context)
                            .load(uri + detailsFilms.getBackdropPath())
                            .into(imageView);
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }
            @Override
            public void onFailure(Call<DetailsFilms> call, Throwable t) {
            }
        });
    }


    private void getCreditsFilm(final ApiInterface service) {
        Call<Credits> call = service.getCredits(id);
        call.enqueue(new Callback<Credits>() {
            @Override
            public void onResponse(Call<Credits> call, Response<Credits> response) {
                if (response.isSuccessful()) {
                    credits = response.body();
                    RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_film_credits);
                    mRecyclerView.setHasFixedSize(true);

                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    RAdapterCredits = new RecyclerAdapterCredits(credits, context, service);
                    mRecyclerView.setAdapter(RAdapterCredits);
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<Credits> call, Throwable t) {

            }

        });
    }

    private void loadTrailer(ApiInterface service) {
        Call<TrilerFilm> call = service.getTrailer(id);
        call.enqueue(new Callback<TrilerFilm>() {
            @Override
            public void onResponse(Call<TrilerFilm> call, Response<TrilerFilm> response) {
                if (response.isSuccessful()) {

                    trilerFilm = response.body();

                    try {
                        resultTtiler = trilerFilm.getResultTtiler().get(0);
                        idTriler = resultTtiler.getKey();
                        key = resultTtiler.getId();
                    }catch (IndexOutOfBoundsException e){
                        idTriler = null;
                        key = null;
                    }




                    if (idTriler != null && key != null){
                        youTubePlayerView.initialize(key, oil);
                    }


                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<TrilerFilm> call, Throwable t) {

            }


        });
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult err) {
        Toast.makeText(this, "Нет доступа к сети" + err.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (idTriler != null){
        youTubePlayer.loadVideo(idTriler);}
    }

    String getGenrsInString(List<Genre> genrs){
        String genrsString = null;
        for (Genre genre: genrs){
            if (genrsString == null) {genrsString = genre.getName();}
            else {genrsString = genrsString + ", " + genre.getName();}
        }
        return genrsString;
    }

    String getProductionCompaniesInString(List<ProductionCompany> companies){
        String genrsString = null;
        for (ProductionCompany company: companies){
            if (genrsString == null) {genrsString = company.getName();}
            else {genrsString = genrsString + ", " + company.getName();}
        }
        return genrsString;
    }

}
