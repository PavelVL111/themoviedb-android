package ru.pavel.themoviedb.Films;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.pavel.themoviedb.ApiInterface;
import ru.pavel.themoviedb.R;
import ru.pavel.themoviedb.TVShows.RecyclerAdapterTV;
import ru.pavel.themoviedb.pojo.Films.PopularFilms;
import ru.pavel.themoviedb.pojo.Films.Result;
import ru.pavel.themoviedb.pojo.ResultTV;
import ru.pavel.themoviedb.pojo.TVPopular;


public class SearchFilms extends AppCompatActivity {
    MaterialSearchView materialSearchView;
    final Context context = this;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerAdapter mAdapter;
    private RecyclerAdapterTV mAdapterTV;
    private String BASE_URL = "https://api.themoviedb.org";
    private RecyclerView mRecyclerView;
    List<Result> listResult;
    PopularFilms result;
    List<ResultTV> listResultTV;
    TVPopular resultTV;
    int flag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_films);
        Intent intent = getIntent();
        flag = intent.getIntExtra("flag",flag);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_search);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Поиск");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        materialSearchView = (MaterialSearchView) findViewById(R.id.search_view);
        materialSearchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener(){
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

            }
        });


        materialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener(){
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Retrofit client = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                final ApiInterface service = client.create(ApiInterface.class);
                if (flag == 1)
                searchFilms(service, newText);
                if (flag == 2)
                    searchTV(service, newText);
                return true;
            }
        });
    }

    void searchFilms(final ApiInterface service, String newText){
        Call<PopularFilms> call = service.getSearch(newText);
        call.enqueue(new Callback<PopularFilms>() {
            @Override
            public void onResponse(Call<PopularFilms> call, Response<PopularFilms> response) {
                if (response.isSuccessful()) {
                    result = response.body();
                    listResult = result.getResults();

                    mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_search);

                    mRecyclerView.setHasFixedSize(true);

                    mLayoutManager = new LinearLayoutManager(SearchFilms.this);
                    mRecyclerView.setLayoutManager(mLayoutManager);



                    mAdapter = new RecyclerAdapter(listResult, context, service);
                    mRecyclerView.setAdapter(mAdapter);

                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<PopularFilms> call, Throwable t) {

            }

        });
    }

    void searchTV(final ApiInterface service, String newText){
        Call<TVPopular> call = service.getSearchTV(newText);
        call.enqueue(new Callback<TVPopular>() {
            @Override
            public void onResponse(Call<TVPopular> call, Response<TVPopular> response) {
                if (response.isSuccessful()) {
                    resultTV = response.body();
                    listResultTV = resultTV.getResults();

                    mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_search);

                    mRecyclerView.setHasFixedSize(true);

                    mLayoutManager = new LinearLayoutManager(SearchFilms.this);
                    mRecyclerView.setLayoutManager(mLayoutManager);

                    mAdapterTV = new RecyclerAdapterTV(listResultTV, context, service);
                    mRecyclerView.setAdapter(mAdapterTV);

                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<TVPopular> call, Throwable t) {

            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search,menu);
        MenuItem item = menu.findItem(R.id.action_search);
        materialSearchView.setMenuItem(item);
        materialSearchView.showSearch();
        return true;
    }



}
