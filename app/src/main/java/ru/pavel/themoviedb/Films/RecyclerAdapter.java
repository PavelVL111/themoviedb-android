package ru.pavel.themoviedb.Films;

/**
 * Created by Pavel on 14.03.2017.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.pavel.themoviedb.ApiInterface;
import ru.pavel.themoviedb.R;
import ru.pavel.themoviedb.pojo.Films.Result;
import ru.pavel.themoviedb.pojo.Films.PopularFilms;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    public Context context;
    Uri uri = Uri.parse("https://image.tmdb.org/t/p/w500/");
    List<Result> listResult;
    private ApiInterface service;


    public RecyclerAdapter(List<Result> dataset, Context context, ApiInterface service) {
        this.listResult = dataset;
        this.context = context;
        this.service = service;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title;
        public TextView originalTitle;
        public TextView releaseDate;
        public TextView voteAverage;
        public TextView originalLanguage;
        public ImageView posterPath;
        public CardView cardView;
        public Context forFragContext;
        List<Result> listResult;
        ApiInterface service;

        public ViewHolder(View v, Context context, List<Result> listResult, ApiInterface service) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            originalTitle = (TextView) v.findViewById(R.id.original_title);
            releaseDate = (TextView) v.findViewById(R.id.release_date_oncard);
            voteAverage = (TextView) v.findViewById(R.id.vote_average_oncard);
           // originalLanguage = (TextView) v.findViewById(R.id.original_language_oncard);
            posterPath = (ImageView) v.findViewById(R.id.poster_path);
            cardView = (CardView) v.findViewById(R.id.card_view);
            forFragContext = context;
            cardView.setOnClickListener(this);
            this.listResult = listResult;
            this.service = service;

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(forFragContext, AboutFilm.class);
            intent.putExtra("title", listResult.get(getAdapterPosition()).getTitle());
            intent.putExtra("id", listResult.get(getAdapterPosition()).getId().toString());
            intent.putExtra("parcelable", listResult.get(getAdapterPosition()));
            forFragContext.startActivity(intent);
        }
    }


    public void add(PopularFilms result) {
        listResult.addAll(result.getResults());
        notifyDataSetChanged();
    }


    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);

        ViewHolder vh = new ViewHolder(v, context, listResult, service);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ImageView imageView = new ImageView(this.context);


        holder.title.setText( listResult.get(position).getTitle());
        holder.originalTitle.setText( listResult.get(position).getOriginalTitle());
        holder.releaseDate.setText("Дата релиза " + listResult.get(position).getReleaseDate().replace('-','.'));
       // holder.originalLanguage.setText("Оригинальный язык " + listResult.get(position).getOriginalLanguage());
        holder.voteAverage.setText(listResult.get(position).getVoteAverage().toString());

        Picasso.with(context)
                .load(uri + listResult.get(position).getBackdropPath())
                .placeholder(R.drawable.null_image)
                .into(holder.posterPath);


      //  holder.cardView.setBackground(imageView.getDrawable());
    }

    @Override
    public int getItemCount() {
        return listResult.size();
    }



}