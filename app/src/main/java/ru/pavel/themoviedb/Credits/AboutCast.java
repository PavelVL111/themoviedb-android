package ru.pavel.themoviedb.Credits;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.pavel.themoviedb.ApiInterface;
import ru.pavel.themoviedb.R;
import ru.pavel.themoviedb.pojo.Films.Result;
import ru.pavel.themoviedb.pojo.Films.ResultTtiler;
import ru.pavel.themoviedb.pojo.Films.TrilerFilm;
import ru.pavel.themoviedb.pojo.DetailsTeam.Cast;

public class AboutCast extends Activity {
    private int position;
    private List<Result> listResult;
    private String BASE_URL = "https://api.themoviedb.org";
    private TextView textView;


    private TextView placeofbirthCast;
    private SimpleDraweeView profilePathCast;
    private TrilerFilm trilerFilm;
    private YouTubePlayer.OnInitializedListener oil;
    private String idTriler;
    private String key;
    private Intent intent;
    private ResultTtiler resultTtiler;
    private Cast detailsCast;
    private Context context;
    private Uri uri = Uri.parse("https://image.tmdb.org/t/p/w600");
    private String id;
    private TextView nameCast;
    private TextView birthdayCast;
    private TextView biographyCast;

    private TextView imdbidCast;
    private TextView deathdayCast;

    private TextView genderCast;
    private TextView popularityCast;
    private YouTubePlayerView youTubePlayerView;
    private LinearLayout linearLayoutDark;
    private LinearLayout linearLayoutWhite;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_credits);
        intent = getIntent();
        context = this;

        id = intent.getStringExtra("idCasts");
        nameCast = (TextView) findViewById(R.id.name_cast);
        biographyCast = (TextView) findViewById(R.id.biography_cast);
        genderCast =  (TextView) findViewById(R.id.gender_cast);
        popularityCast = (TextView) findViewById(R.id.popularity_cast);
        birthdayCast = (TextView) findViewById(R.id.birthday_cast);

        imdbidCast = (TextView) findViewById(R.id.imdbid_cast);
        deathdayCast = (TextView) findViewById(R.id.deathday_cast);

        placeofbirthCast = (TextView) findViewById(R.id.placeofbirth_cast);
        profilePathCast = (SimpleDraweeView) findViewById(R.id.profile_path_cast);
        linearLayoutWhite = (LinearLayout) findViewById(R.id.white_cast);
        Retrofit client = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final ApiInterface service = client.create(ApiInterface.class);

        getDetilsCast(service);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void getDetilsCast(ApiInterface service) {
        Call<Cast> call = service.getDetilsCast(id);
        call.enqueue(new Callback<Cast>() {
            @Override
            public void onResponse(Call<Cast> call, Response<Cast> response) {
                if (response.isSuccessful()) {
                    detailsCast = response.body();
                    nameCast.setText(detailsCast.getName());
                    biographyCast.setText(detailsCast.getBiography());
                    birthdayCast.setText(detailsCast.getBirthday());

                    imdbidCast.setText(detailsCast.getDeathday());
                    genderCast.setText(detailsCast.getGender().toString());
                    popularityCast.setText(detailsCast.getPopularity().toString());
                    deathdayCast.setText(detailsCast.getDeathday());
                    placeofbirthCast.setText(detailsCast.getPlaceOfBirth());



                    profilePathCast.setImageURI(uri + detailsCast.getProfilePath());
                    linearLayoutWhite.setVisibility(View.INVISIBLE);
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<Cast> call, Throwable t) {
            }
        });
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("AboutCast Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2.connect();
        AppIndex.AppIndexApi.start(client2, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client2, getIndexApiAction());
        client2.disconnect();
    }
}
