package ru.pavel.themoviedb.Credits;

/**
 * Created by Pavel on 14.03.2017.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.pavel.themoviedb.ApiInterface;
import ru.pavel.themoviedb.R;
import ru.pavel.themoviedb.pojo.Team.Cast;
import ru.pavel.themoviedb.pojo.Team.Credits;
import ru.pavel.themoviedb.pojo.Team.Crew;


public class RecyclerAdapterCredits extends RecyclerView.Adapter<RecyclerAdapterCredits.ViewHolder> {
    public Context context;
    Uri uri = Uri.parse("https://image.tmdb.org/t/p/w500");
    Credits credits;
    private ApiInterface service;
    private List<Cast> casts;
    private List<Crew> crews;

    public RecyclerAdapterCredits(Credits credits, Context context, ApiInterface service) {
        this.credits = credits;
        this.context = context;
        this.service = service;
        casts = credits.getCast();
        crews = credits.getCrew();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        public TextView originalTitle;
        public TextView releaseDate;
        public TextView voteAverage;
        public TextView originalLanguage;
        public ImageView profilePath;
        public CardView cardView;
        public Context forFragContext;
        Credits credits;
        ApiInterface service;

        public ViewHolder(View v, Context context, Credits credits, ApiInterface service) {
            super(v);
            name = (TextView) v.findViewById(R.id.name_cast);
            cardView = (CardView) v.findViewById(R.id.card_credits_view);
            profilePath = (ImageView) v.findViewById(R.id.profile_path_credits);
            forFragContext = context;
            cardView.setOnClickListener(this);
            this.credits = credits;
            this.service = service;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(forFragContext, AboutCast.class);
//            intent.putExtra("title", listResult.get(getAdapterPosition()).getTitle());
//            intent.putExtra("id", listResult.get(getAdapterPosition()).getId().toString());
            intent.putExtra("idCasts", credits.getCast().get(getAdapterPosition()).getId().toString());
            forFragContext.startActivity(intent);
        }
    }



    @Override
    public RecyclerAdapterCredits.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_credits_item, parent, false);

        ViewHolder vh = new ViewHolder(v, context, credits, service);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ImageView imageView = new ImageView(this.context);


        holder.name.setText( casts.get(position).getName());


        Picasso.with(context)
                .load(uri + casts.get(position).getProfilePath())
                .placeholder(R.drawable.face)
                .into(holder.profilePath);


      //  holder.cardView.setBackground(imageView.getDrawable());
    }

    @Override
    public int getItemCount() {
        return casts.size();
    }



}