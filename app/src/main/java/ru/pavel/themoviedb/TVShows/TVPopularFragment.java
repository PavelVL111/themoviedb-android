package ru.pavel.themoviedb.TVShows;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.pavel.themoviedb.ApiInterface;
import ru.pavel.themoviedb.R;
import ru.pavel.themoviedb.pojo.Films.Result;
import ru.pavel.themoviedb.pojo.ResultTV;
import ru.pavel.themoviedb.pojo.TVPopular;


public class TVPopularFragment  extends Fragment {

    private Context context;
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerViewNew;
    private LinearLayoutManager mLayoutManager;
    private LinearLayoutManager mLayoutManagerNew;
    //private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerAdapterTV mAdapter;
    private ApiInterface service;
    private String BASE_URL = "https://api.themoviedb.org";
    private Uri uri = Uri.parse("https://image.tmdb.org/t/p/w500/");
    private String page;
    private TextView textView;
    private Result result1;
    private List<ResultTV> listResult;
    private TVPopular result;
    private Toolbar toolbartab;
    private int totalItem;
    private int lastVisibleItem = 0;
    private int pg = 1;
    private int lastValue = 0;
    private View view;
    private ProgressBar progressBar;
    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            totalItem = mLayoutManager.getItemCount();
            lastVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
            if (lastValue != lastVisibleItem){
                if ((totalItem - 3) == lastVisibleItem){
                    pg++;
                    setPageCall(pg + "");
                }
                lastValue = lastVisibleItem;
            }

        }
    };


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_tvpopular, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar_tv);
        runClient("1");
        return view;
    }


    private void runClient(String page) {
        Retrofit client = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = client.create(ApiInterface.class);
        setFirstPageCall(page);
    }

    private void setFirstPageCall(String page) {
        Call<TVPopular> call = service.getTVPopular(page);
        startCall(call);
    }

    private void startCall(Call<TVPopular> call) {
        call.enqueue(new Callback<TVPopular>() {
            @Override
            public void onResponse(Call<TVPopular> call, Response<TVPopular> response) {
                if (response.isSuccessful()) {
                    result = response.body();
                    listResult = result.getResults();

                    mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_tv_popular);
                    mRecyclerView.setHasFixedSize(true);

                    mLayoutManager = new LinearLayoutManager(view.getContext());
                    mRecyclerView.setLayoutManager(mLayoutManager);

                    mAdapter = new RecyclerAdapterTV(listResult, view.getContext(), service);
                    mRecyclerView.setAdapter(mAdapter);
                    mRecyclerView.addOnScrollListener(onScrollListener);
                    progressBar.setVisibility(ProgressBar.INVISIBLE);

                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<ru.pavel.themoviedb.pojo.TVPopular> call, Throwable t) {

            }


        });
    }

    private void setPageCall(String page) {
        Call<TVPopular> call = service.getTVPopular(page);
        addCall(call);
    }

    private void addCall(Call<ru.pavel.themoviedb.pojo.TVPopular> call) {
        call.enqueue(new Callback<ru.pavel.themoviedb.pojo.TVPopular>() {
            @Override
            public void onResponse(Call<ru.pavel.themoviedb.pojo.TVPopular> call, Response<ru.pavel.themoviedb.pojo.TVPopular> response) {
                if (response.isSuccessful()) {
                    result = response.body();
                    mAdapter.add(result);
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<ru.pavel.themoviedb.pojo.TVPopular> call, Throwable t) {

            }

        });
    }
}