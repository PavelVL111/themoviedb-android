package ru.pavel.themoviedb.TVShows;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.pavel.themoviedb.ApiInterface;
import ru.pavel.themoviedb.Credits.RecyclerAdapterCredits;
import ru.pavel.themoviedb.R;
import ru.pavel.themoviedb.pojo.DetailsAboutTV.DetailsTV;
import ru.pavel.themoviedb.pojo.DetailsAboutTV.Video.VideoTV;
import ru.pavel.themoviedb.pojo.Films.Result;
import ru.pavel.themoviedb.pojo.Films.ResultTtiler;
import ru.pavel.themoviedb.pojo.Films.TrilerFilm;
import ru.pavel.themoviedb.pojo.ResultTV;
import ru.pavel.themoviedb.pojo.Team.Credits;

import static java.lang.System.err;

public class AboutTV extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{
    private int position;
    private List<Result> listResult;
    private String BASE_URL = "https://api.themoviedb.org";
    private TextView textView;
    private TrilerFilm trilerFilm;
    private YouTubePlayer.OnInitializedListener oil;
    private String idTriler;
    private String key;
    private Intent intent;
    private ResultTtiler resultTtiler;
    private String id;
    private DetailsTV detailsTV ;
    private Context context;
    private Uri uri = Uri.parse("https://image.tmdb.org/t/p/w500");
    private ImageView imageView;
    private ImageView imagePlay;
    private TextView name;
    private YouTubePlayerView youTubePlayerView;
    private LinearLayout linearLayoutDark;
    private LinearLayout linearLayoutWhite;
    private ApiInterface service;
    private RecyclerAdapterCredits RAdapterCredits;
    private Credits credits;
    private LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_about_tv);
        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youTubePlayerView);
        intent = getIntent();
        ResultTV result = intent.getParcelableExtra("parcelabletv");
        id = result.getId().toString();
        context = this;
        oil = this;
        imageView = (ImageView) findViewById(R.id.backdrop_path_tv);
        imagePlay = (ImageView) findViewById(R.id.play_tv);
        linearLayoutDark = (LinearLayout) findViewById(R.id.dark_tv);
        name = (TextView) findViewById(R.id.name_tv);
        linearLayoutWhite = (LinearLayout) findViewById(R.id.white_tv);
        Retrofit client = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = client.create(ApiInterface.class);
        imagePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setVisibility(View.INVISIBLE);
                imagePlay.setVisibility(View.INVISIBLE);
                linearLayoutDark.setVisibility(View.INVISIBLE);
                youTubePlayerView.setVisibility(View.VISIBLE);
                loadTrailer(service);
            }
        });
        getDetilsTV(service);
        getCreditsFilm();
        youTubePlayerView.setVisibility(View.INVISIBLE);
    }


    private void loadTrailer(ApiInterface service) {
        Call<VideoTV> call = service.getVideoTV(id);
        call.enqueue(new Callback<VideoTV>() {
            @Override
            public void onResponse(Call<VideoTV> call, Response<VideoTV> response) {
                if (response.isSuccessful()) {

                    VideoTV videoTV = response.body();
                    try {
                        key = videoTV.getResults().get(0).getId();
                        idTriler = videoTV.getResults().get(0).getKey();
                    }catch (IndexOutOfBoundsException e){
                        idTriler = null;
                        key = null;
                    }
                    if (idTriler != null && key != null){
                        youTubePlayerView.initialize(key, oil);
                    }
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<VideoTV> call, Throwable t) {

            }


        });
    }

    private void getDetilsTV(ApiInterface service) {
        Call<DetailsTV> call = service.getDetilsTV(id);
        call.enqueue(new Callback<DetailsTV>() {
            @Override
            public void onResponse(Call<DetailsTV> call, Response<DetailsTV> response) {
                if (response.isSuccessful()) {
                    detailsTV = response.body();
                    name.setText(detailsTV.getName());

                    Picasso.with(context)
                            .load(uri + detailsTV.getBackdropPath())
                            .into(imageView);
                    linearLayoutWhite.setVisibility(View.INVISIBLE);
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }
            @Override
            public void onFailure(Call<DetailsTV> call, Throwable t) {
            }
        });
    }

    private void getCreditsFilm() {
        Call<Credits> call = service.getCredits(id);
        call.enqueue(new Callback<Credits>() {
            @Override
            public void onResponse(Call<Credits> call, Response<Credits> response) {
                if (response.isSuccessful()) {
                    credits = response.body();
                    RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_tv_credits);
                    mRecyclerView.setHasFixedSize(true);

                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    RAdapterCredits = new RecyclerAdapterCredits(credits, context, service);
                    mRecyclerView.setAdapter(RAdapterCredits);
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<Credits> call, Throwable t) {

            }

        });
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (idTriler != null){
            youTubePlayer.loadVideo(idTriler);}
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Нет доступа к сети" + err.toString(), Toast.LENGTH_LONG).show();
    }
}
