package ru.pavel.themoviedb;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.pavel.themoviedb.pojo.DetailsAboutTV.DetailsTV;
import ru.pavel.themoviedb.pojo.DetailsAboutTV.Video.VideoTV;
import ru.pavel.themoviedb.pojo.Films.PopularFilms;
import ru.pavel.themoviedb.pojo.DetailsAboutFilms.DetailsFilms;
import ru.pavel.themoviedb.pojo.*;
import ru.pavel.themoviedb.pojo.Films.TrilerFilm;
import ru.pavel.themoviedb.pojo.Team.Cast;
import ru.pavel.themoviedb.pojo.Team.Credits;

/**
 * Created by Pavel on 11.03.2017.
 */

public interface ApiInterface {

    @GET("/3/movie/popular?api_key=36ef93cced64d453ec991d668f048e73&language=ru-US")
    Call<PopularFilms> getFilm(@Query("page") String number);


    @GET("/3/movie/{idMovie}/videos?api_key=36ef93cced64d453ec991d668f048e73&language=ru-US")
    Call<TrilerFilm> getTrailer(@Path("idMovie") String idMovie);
    //&query={nameFilm}

    @GET("/3/movie/{idMovie}?api_key=36ef93cced64d453ec991d668f048e73&language=ru-US")
    Call<DetailsFilms> getDetilsFilm(@Path("idMovie") String idMovie);

    @GET("/3/search/movie?include_adult=false&page=1&language=ru-US&api_key=36ef93cced64d453ec991d668f048e73")
    Call<PopularFilms> getSearch(@Query("query") String nameFilm);

    @GET("/3/search/tv?include_adult=false&page=1&language=ru-US&api_key=36ef93cced64d453ec991d668f048e73")
    Call<TVPopular> getSearchTV(@Query("query") String nameFilm);

    @GET("/3/movie/now_playing?api_key=36ef93cced64d453ec991d668f048e73&language=ru-US&region=RU")
    Call<PopularFilms> getNewFilm(@Query("page") String number);

    @GET("/3/tv/popular?api_key=36ef93cced64d453ec991d668f048e73&language=ru-US")
    Call<TVPopular> getTVPopular(@Query("page") String number);

    @GET("/3/tv/top_rated?api_key=36ef93cced64d453ec991d668f048e73&language=ru-US")
    Call<TVPopular> getTVBest(@Query("page") String number);

    @GET("/3/tv/{idMovie}?api_key=36ef93cced64d453ec991d668f048e73&language=ru-US")
    Call<DetailsTV> getDetilsTV(@Path("idMovie") String idMovie);

    @GET("/3/movie/{idMovie}/credits?api_key=36ef93cced64d453ec991d668f048e73&language=ru-US")
    Call<Credits> getCredits(@Path("idMovie") String idMovie);

    @GET("/3/person/{person_id}?api_key=36ef93cced64d453ec991d668f048e73&language=ru-US")
    Call<ru.pavel.themoviedb.pojo.DetailsTeam.Cast> getDetilsCast(@Path("person_id") String personId);

    @GET("/3/tv/{tv_id}/videos?api_key=36ef93cced64d453ec991d668f048e73")
    Call<VideoTV> getVideoTV(@Path("tv_id") String tvId);
}
